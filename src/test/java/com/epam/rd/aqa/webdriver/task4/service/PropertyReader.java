package com.epam.rd.aqa.webdriver.task4.service;

import com.epam.rd.aqa.webdriver.task4.model.PriceCalculator;
import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.InputStream;

public class PropertyReader {

    private PropertyReader() {
    }

    public static PriceCalculator read(String environment) {
        Yaml yaml = new Yaml(new Constructor(PriceCalculator.class, new LoaderOptions()));
        InputStream in = PriceCalculator.class.getClassLoader()
                .getResourceAsStream(environment + ".yaml");
        return yaml.load(in);
    }

}
