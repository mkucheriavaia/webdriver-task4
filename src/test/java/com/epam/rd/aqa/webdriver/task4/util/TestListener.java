package com.epam.rd.aqa.webdriver.task4.util;

import com.epam.rd.aqa.webdriver.task4.framework.TestFrameworkDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TestListener implements ITestListener {

    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss_zz");

    public void onTestFailure(ITestResult result) {
        takeScreenShot();
    }

    private void takeScreenShot() {
        byte[] screenshot = ((TakesScreenshot) TestFrameworkDriver.getDriver()).getScreenshotAs(OutputType.BYTES);
        Path outFile = Path.of(String.format(".//target/screenshots/%s.png", ZonedDateTime.now().format(FORMATTER)));
        try {
            Files.createDirectories(outFile.getParent());
            Files.write(outFile, screenshot, StandardOpenOption.CREATE_NEW);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
