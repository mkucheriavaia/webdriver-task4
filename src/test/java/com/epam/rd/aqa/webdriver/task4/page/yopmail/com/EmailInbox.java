package com.epam.rd.aqa.webdriver.task4.page.yopmail.com;

import com.epam.rd.aqa.webdriver.task4.page.cloud.google.com.PricingCalculatorPage;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.Set;

public class EmailInbox {

    private static final By FIRST_EMAIL_LOCATOR = By.xpath("//div[@class='m'][1]");

    @FindBy(css = "div.bname")
    private WebElement email;

    @FindBy(css = "button#refresh")
    private WebElement refreshBnt;

    @FindBy(xpath = "//iframe[@name='ifinbox']")
    private WebElement inboxIframe;

    @FindBy(xpath = "//iframe[@name='ifmail']")
    private WebElement mailIframe;

    @FindBy(css = "div#mail")
    private WebElement emailContent;

    private final WebDriver driver;

    public EmailInbox(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getGeneratedEmail() {
        return email.getText().trim();
    }

    public PricingCalculatorPage switchBackToPriceCalculator() {
        Set<String> tabs = driver.getWindowHandles();
        tabs.remove(driver.getWindowHandle()); // should left only price calc page handle

        return new PricingCalculatorPage(driver.switchTo().window(tabs.toArray(new String[0])[0]))
                .switchToTabsIframe();
    }

    public EmailInbox waitForEstimationEmail() {
        refreshBnt.click();

        for (int i = 0; i < 10; i++) {
            driver.switchTo().frame(inboxIframe);
            try {
                Optional<WebElement> webElement = Optional.ofNullable(
                        new WebDriverWait(driver, Duration.of(3, ChronoUnit.SECONDS))
                                .until(ExpectedConditions.presenceOfElementLocated(FIRST_EMAIL_LOCATOR)));
                if (webElement.isPresent()) {
                    webElement.get().click();
                    driver.switchTo().parentFrame();
                    break;
                }
            } catch (TimeoutException e) {
            }
            driver.switchTo().parentFrame();
            refreshBnt.click();
        }
        return this;
    }

    public String getEmailBodyText() {
        driver.switchTo().frame(mailIframe);
        return emailContent.getText();
    }
}
