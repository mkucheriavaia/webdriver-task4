package com.epam.rd.aqa.webdriver.task4.test;

import com.epam.rd.aqa.webdriver.task4.framework.CommonTest;
import com.epam.rd.aqa.webdriver.task4.model.PriceCalculator;
import com.epam.rd.aqa.webdriver.task4.page.cloud.google.com.HomePage;
import com.epam.rd.aqa.webdriver.task4.page.cloud.google.com.PricingCalculatorPage;
import com.epam.rd.aqa.webdriver.task4.page.cloud.google.com.price_calculator.tab.ComputeEngine;
import com.epam.rd.aqa.webdriver.task4.page.yopmail.com.EmailInbox;
import com.epam.rd.aqa.webdriver.task4.service.PropertyReader;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CloudGoogleComTest extends CommonTest {

    private final PriceCalculator priceCalculator;

    @Parameters({"environment"})
    public CloudGoogleComTest(String environment) {
        priceCalculator = PropertyReader.read(environment);
    }

    @Test
    public void testCloudGoogleCom() {

        ComputeEngine computeEngineTab = new HomePage(getDriver())
                .openPage()
                .searchFor("Google Cloud Platform Pricing Calculator")
                .followResultLink("Google Cloud Pricing Calculator", PricingCalculatorPage.class)
                .selectTab(priceCalculator.getTabName(), ComputeEngine.class)
                .fillOutForm(priceCalculator.getComputeEngine())
                .clickInstanceAddToEstimate()
                .clickEmailEstimate();

        String actualTotalEstimate = computeEngineTab.getTotalEstimate();
        Matcher matcher = Pattern.compile("Total Estimated Cost: (USD [\\d.,]+)\\s+per 1 month")
                .matcher(actualTotalEstimate);
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(matcher.matches());

        EmailInbox emailInbox = computeEngineTab
                .switchToNewTab(com.epam.rd.aqa.webdriver.task4.page.yopmail.com.HomePage.class)
                .generateRandomEmail();

        String generatedEmail = emailInbox.getGeneratedEmail();

        emailInbox = emailInbox.switchBackToPriceCalculator()
                .sendEmailWithEstimation(generatedEmail)
                .switchToEmailTab()
                .waitForEstimationEmail();
        String emailBodyText = emailInbox.getEmailBodyText();
        softAssert.assertTrue(Pattern.matches("(?s)^.*Total Estimated Monthly Cost\\s+" + matcher.group(1) + "\\s*\\n.*$", emailBodyText));
        softAssert.assertAll();
    }
}