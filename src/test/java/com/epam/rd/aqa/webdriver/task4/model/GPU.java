package com.epam.rd.aqa.webdriver.task4.model;

import java.util.Objects;

public class GPU {
    private String type;
    private int count;

    public GPU() {}

    public GPU(String type, int count) {
        this.type = type;
        this.count = count;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "GPU{" +
                "type='" + type + '\'' +
                ", count=" + count +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GPU gpu = (GPU) o;
        return count == gpu.count && Objects.equals(type, gpu.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, count);
    }
}
