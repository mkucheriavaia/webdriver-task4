package com.epam.rd.aqa.webdriver.task4.page.cloud.google.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class SearchResultPage {

    private final WebDriver driver;

    public SearchResultPage(WebDriver driver) {
        this.driver = driver;
    }

    public <T> T followResultLink(String linkText, Class<T> expectedPageObjectClass) {
        Optional.ofNullable(
                new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS))
                        .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='gs-title']/a[normalize-space()='" + linkText + "']"))))
                .ifPresent(WebElement::click);
        try {
            return expectedPageObjectClass.getConstructor(WebDriver.class).newInstance(driver);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
