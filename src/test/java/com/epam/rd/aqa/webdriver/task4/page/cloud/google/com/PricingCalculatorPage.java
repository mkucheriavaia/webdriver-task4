package com.epam.rd.aqa.webdriver.task4.page.cloud.google.com;

import com.epam.rd.aqa.webdriver.task4.page.yopmail.com.EmailInbox;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;

public class PricingCalculatorPage {

    @FindBy(css = "article#cloud-site iframe")
    private WebElement cloudSiteIframe;

    @FindBy(id = "myFrame")
    private WebElement tabsIframe;

    @FindBy(xpath = "//input[@ng-model='emailQuote.user.email']")
    private WebElement userEmail;

    @FindBy(xpath = "//button[normalize-space()='Send Email'][not(@disabled)]")
    private WebElement sendEmailBnt;

    private final WebDriver driver;

    public PricingCalculatorPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public PricingCalculatorPage switchToTabsIframe() {
        driver.switchTo().frame(cloudSiteIframe).switchTo().frame(tabsIframe);
        return this;
    }

    public <T> T selectTab(String tabName, Class<T> expectedTabClass) {
        switchToTabsIframe();

        driver.findElement(By.xpath("//md-tab-item//div[@class='name']/span[text()='" + tabName + "']/../../../.."))
                .click();

        try {
            return expectedTabClass.getConstructor(WebDriver.class).newInstance(driver);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public PricingCalculatorPage sendEmailWithEstimation(String email) {
        userEmail.sendKeys(email);
        sendEmailBnt.click();
        return this;
    }

    public EmailInbox switchToEmailTab() {
        Set<String> tabs = driver.getWindowHandles();
        tabs.remove(driver.getWindowHandle()); // should left only price calc page handle

        return new EmailInbox(driver.switchTo().window(tabs.toArray(new String[0])[0]));
    }
}
