package com.epam.rd.aqa.webdriver.task4.framework;

import com.epam.rd.aqa.webdriver.task4.util.TestListener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;

@Listeners({TestListener.class})
public class CommonTest {
    private WebDriver driver;

    @BeforeClass
    @Parameters({"browser"})
    public void setUp(String browser) {
        driver = TestFrameworkDriver.getDriver(browser);
    }

    @AfterClass
    public void cleanUp() {
        TestFrameworkDriver.cleanUp();
    }

    public WebDriver getDriver() {
        return driver;
    }
}
