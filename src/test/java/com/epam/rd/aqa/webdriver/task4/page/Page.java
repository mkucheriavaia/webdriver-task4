package com.epam.rd.aqa.webdriver.task4.page;

public interface Page<T> {
    T openPage();
}
