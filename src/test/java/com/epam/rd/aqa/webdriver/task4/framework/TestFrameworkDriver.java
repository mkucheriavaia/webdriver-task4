package com.epam.rd.aqa.webdriver.task4.framework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestFrameworkDriver {
    private static WebDriver driver;

    private TestFrameworkDriver() {}

    public static WebDriver getDriver() {
        return driver;
    }

    public static WebDriver getDriver(String browser) {
        if (driver == null) {
            switch (browser) {
                case "chrome" -> {
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                }
                case "firefox" -> {
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                }
                default -> throw new IllegalArgumentException("Wrong browser is provided!");
            }
            driver.manage().window().maximize();
        }
        return driver;
    }

    public static void cleanUp() {
        if (driver == null) return;
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle).close();
        }
        driver.quit();
        driver = null;
    }
}
