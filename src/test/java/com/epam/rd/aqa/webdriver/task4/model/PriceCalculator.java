package com.epam.rd.aqa.webdriver.task4.model;

public class PriceCalculator {
    private String tabName;
    private ComputeEngine computeEngine;

    public PriceCalculator() {
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public ComputeEngine getComputeEngine() {
        return computeEngine;
    }

    public void setComputeEngine(ComputeEngine computeEngine) {
        this.computeEngine = computeEngine;
    }

    @Override
    public String toString() {
        return "PriceCalculator{" +
                "tabName='" + tabName + '\'' +
                ", computeEngine=" + computeEngine +
                '}';
    }
}
