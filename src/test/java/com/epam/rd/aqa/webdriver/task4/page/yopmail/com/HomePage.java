package com.epam.rd.aqa.webdriver.task4.page.yopmail.com;

import com.epam.rd.aqa.webdriver.task4.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class HomePage implements Page<HomePage> {

    public static final String HTTPS_YOPMAIL_COM = "https://yopmail.com/";
    public static final By COOKIES_AGREE_BTN_LOCATOR = By.xpath("//button[@id='accept']");

    @FindBy(xpath = "//a[@href='email-generator']")
    private WebElement randomEmail;

    @FindBy(xpath = "//button/span[normalize-space()='Check Inbox']")
    private WebElement checkInbox;

    private final WebDriver driver;

    public HomePage() {
        this(null);
    }

    public HomePage(WebDriver driver) {
        this.driver = Optional.ofNullable(driver).orElseGet(ChromeDriver::new);
    }

    @Override
    public HomePage openPage() {
        driver.get(HTTPS_YOPMAIL_COM);

        Optional.ofNullable(
                        new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS))
                                .until(ExpectedConditions.elementToBeClickable(COOKIES_AGREE_BTN_LOCATOR)))
                .ifPresent(WebElement::click);

        PageFactory.initElements(driver, this);
        return this;
    }

    public EmailInbox generateRandomEmail() {
        randomEmail.click();
        driver.findElements(By.cssSelector("ins.adsbygoogle")).forEach(w -> ((JavascriptExecutor) driver).executeScript("arguments[0].style.display='none'", w));
        checkInbox.click();
        return new EmailInbox(driver);
    }
}
