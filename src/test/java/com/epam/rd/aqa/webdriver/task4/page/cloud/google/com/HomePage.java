package com.epam.rd.aqa.webdriver.task4.page.cloud.google.com;

import com.epam.rd.aqa.webdriver.task4.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class HomePage implements Page<HomePage> {

    public static final String HTTPS_CLOUD_GOOGLE_COM = "https://cloud.google.com/";
    public static final By COOKIES_AGREE_BTN_LOCATOR = By.xpath("//div[@data-type='cookie-notification']//button");

    @FindBy(xpath = "//form[@action='https://cloud.google.com/s/results']/div")
    private WebElement searchMGField;

    @FindBy(xpath = "//input[@name='q']")
    private WebElement searchField;

    private final WebDriver driver;

    public HomePage() {
        this(null);
    }

    public HomePage(WebDriver driver) {
        this.driver = Optional.ofNullable(driver).orElseGet(ChromeDriver::new);
    }

    @Override
    public HomePage openPage() {
        driver.get(HTTPS_CLOUD_GOOGLE_COM);

        Optional.ofNullable(
                new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS))
                        .until(ExpectedConditions.elementToBeClickable(COOKIES_AGREE_BTN_LOCATOR)))
                .ifPresent(WebElement::click);

        PageFactory.initElements(driver, this);
        return this;
    }

    public SearchResultPage searchFor(String searchTerm) {
        searchMGField.click();

        searchField.sendKeys(searchTerm);
        searchField.sendKeys(Keys.ENTER);

        return new SearchResultPage(driver);
    }
}
